﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class EmployeeDataService : IEmployeeDataService
    {
        private readonly IRepository<Employee> _repository;
        
        public EmployeeDataService(IRepository<Employee> repository)
        {
            _repository = repository;
        }
        
        public async Task<IEnumerable<Employee>> GetAllAsync() => 
            await _repository.GetAllAsync();
        
        public async Task<Employee> GetByIdAsync(Guid id) =>
            await _repository.GetByIdAsync(id);
        
        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _repository.GetByIdAsync(id);
            
            employee.AppliedPromocodesCount++;

            await _repository.UpdateAsync(employee);
        }
    }
}
