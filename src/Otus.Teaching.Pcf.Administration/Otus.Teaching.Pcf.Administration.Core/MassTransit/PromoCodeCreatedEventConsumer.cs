﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.MassTransit.Contracts;

namespace Otus.Teaching.Pcf.Administration.Core.MassTransit
{
    public class PromoCodeCreatedEventConsumer : IConsumer<PromoCodeCreatedContract>
    {
        private readonly IEmployeeDataService _dataService;
        private readonly ILogger<PromoCodeCreatedEventConsumer> _logger;

        public PromoCodeCreatedEventConsumer(IEmployeeDataService dataService, ILogger<PromoCodeCreatedEventConsumer> logger)
        {
            _dataService = dataService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<PromoCodeCreatedContract> context)
        {
            if (context.Message.PartnerManagerId == null)
                throw new ArgumentNullException(nameof(context.Message.PartnerManagerId));

            _logger.LogInformation("PartnerManagerId: {0}", context.Message.PartnerManagerId);

            await _dataService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
            
        }
    }
}
